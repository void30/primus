from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from .views import *

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),

    path('signup/', Signup.as_view()),
    path('signin/', SignIn.as_view()),

    path('children/', ChildrenList.as_view()),
    path('child/<int:pk>', ChildDetail.as_view()),

    path('pi/', RaspberryPiList.as_view()),
    path('pi/<int:pk>', RaspberryPiDetail.as_view()),
    path('pi/updateip/', RaspberryPiUpdateIP.as_view()),
    path('pi/getip/', GetRaspberryPiIP.as_view()),

    path('temp/', TemperatureList.as_view()),
    path('temp/<int:pk>', TemperatureDetail.as_view()),
    path('child-temperature/<int:child_id>', ChildTemperature.as_view()),
    path('child-last-temperature/<int:child_id>', ChildLastTemperature.as_view()),

    path('pulse/', PulseRateList.as_view()),
    path('pulse/<int:pk>', PulseRateDetail.as_view()),
    path('child-pulse-rate/<int:child_id>', ChildPulseRates.as_view()),
    path('child-last-pulse-rate/<int:child_id>', ChildLastPulseRate.as_view()),

    path('fell/', Fell.as_view()),
    path('fell/<int:pk>', FallDetails.as_view()),
    path('child-fell/<int:child_id>', ChildFell.as_view()),
    path('child-last-fall/<int:child_id>', ChildLastFall.as_view()),

    path('child-health-history/<int:child_id>', ChildHealthHistory.as_view()),
]
