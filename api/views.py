from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt import views as jwt_views

from .models import Temperature, PulseRate, Child, Fall, Parent, RaspberryPi
from .serializers import TemperatureSerializer, PulseRateSerializer, ChildSerializer, FallSerializer, \
    RaspberryPiSerializer


class TemperatureList(generics.ListCreateAPIView):
    """
        get:
            Return a list of all temperature records
        post:
            Create a new temperature record
    """
    permission_classes = [IsAuthenticated]

    queryset = Temperature.objects.all()
    serializer_class = TemperatureSerializer


class TemperatureDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        get:
            Return an existing temperature record
        put:
            Update an existing temperature record
        patch:
            Partial update an existing temperature record
        delete:
            Delete an existing temperature record
    """
    permission_classes = [IsAuthenticated]

    queryset = Temperature.objects.all()
    serializer_class = TemperatureSerializer


class PulseRateList(generics.ListCreateAPIView):
    """
        get:
            Return a list of all pulse rate records
        post:
            Create a new pulse rate record
    """
    permission_classes = [IsAuthenticated]

    queryset = PulseRate.objects.all()
    serializer_class = PulseRateSerializer


class PulseRateDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        get:
            Return an existing pulse rate record
        put:
            Update an existing pulse rate record
        patch:
            Partial update an existing pulse rate record
        delete:
            Delete an existing pulse rate record
    """
    permission_classes = [IsAuthenticated]

    queryset = PulseRate.objects.all()
    serializer_class = PulseRateSerializer


class ChildrenList(generics.ListCreateAPIView):
    """
        get:
            Return a list of all children records
        post:
            Create a new child record
    """
    permission_classes = [IsAuthenticated]

    queryset = Child.objects.all()
    serializer_class = ChildSerializer


class ChildDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        get:
            Return an existing child record
        put:
            Update an existing child record
        patch:
            Partial update an existing child record
        delete:
            Delete an existing child record
    """
    permission_classes = [IsAuthenticated]

    queryset = Child.objects.all()
    serializer_class = ChildSerializer


class RaspberryPiList(generics.ListCreateAPIView):
    """
        get:
            Return a list of all Raspberry Pis records
        post:
            Create a new Raspberry Pi record
    """
    queryset = RaspberryPi.objects.all()
    serializer_class = RaspberryPiSerializer


class RaspberryPiDetail(generics.RetrieveUpdateDestroyAPIView):
    """
        get:
            Return an existing Raspberry Pi record
        put:
            Update an existing Raspberry Pi record
        patch:
            Partial update an existing Raspberry Pi record
        delete:
            Delete an existing Raspberry Pi record
    """
    queryset = RaspberryPi.objects.all()
    serializer_class = RaspberryPiSerializer


class RaspberryPiUpdateIP(APIView):
    """
        Update the current IP associated with a given mac address.
    """
    def put(self, request, **kwargs):

        mac = request.data['mac_address']
        raspberry = RaspberryPi.objects.filter(mac_address=mac)

        if not raspberry:
            return JsonResponse({"response": "Mac Address not found"}, status=status.HTTP_404_NOT_FOUND)

        ip = request.data["VPN_ip"]
        raspberry[0].VPN_ip = ip
        raspberry[0].save()

        return JsonResponse({"response": "IP Updated"}, status=status.HTTP_202_ACCEPTED)


class GetRaspberryPiIP(APIView):
    """
        Retrieve the IP Address of a given MAC Address.
    """
    permission_classes = [IsAuthenticated]

    def post(self, request, **kwargs):
        mac = request.data['mac_address']
        raspberry = RaspberryPi.objects.filter(mac_address=mac)

        if not raspberry:
            return JsonResponse({"response": "Mac Address not found"}, status=status.HTTP_404_NOT_FOUND)

        return JsonResponse({"VPN_ip": raspberry[0].VPN_ip}, status=status.HTTP_200_OK)


class ChildPulseRates(generics.ListAPIView):
    """
        get:
            Return a list of all pulse rate records for a certain child
    """
    serializer_class = PulseRateSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        child = self.kwargs['child_id']
        return PulseRate.objects.filter(child_id=child)


class ChildLastPulseRate(APIView):
    """Return last recorded Temperature for a child"""
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        child = kwargs['child_id']
        query = PulseRate.objects.filter(child_id=child).latest('created_at')
        serializer = PulseRateSerializer(query, many=False)
        return JsonResponse(serializer.data, safe=False)


class ChildTemperature(generics.ListAPIView):
    """
        get:
            Return a list of all temperature records for a certain child
    """
    serializer_class = TemperatureSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        child = self.kwargs['child_id']
        return Temperature.objects.filter(child_id=child)


class ChildLastTemperature(APIView):
    """Return last recorded Temperature for a child"""
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        child = kwargs['child_id']
        query = Temperature.objects.filter(child_id=child).latest('created_at')
        serializer = TemperatureSerializer(query, many=False)
        return JsonResponse(serializer.data, safe=False)


class ChildHealthHistory(APIView):
    """Return a single json object contains both temperature and pulse rate history for a child"""
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        child = kwargs['child_id']
        pulse_query = PulseRate.objects.filter(child_id=child)
        temp_query = Temperature.objects.filter(child_id=child)

        pulse_rate_serializer = PulseRateSerializer(pulse_query, many=True)
        temp_serializer = TemperatureSerializer(temp_query, many=True)

        data = {
            'temp': temp_serializer.data,
            'pulse': pulse_rate_serializer.data,
        }

        return JsonResponse(data, safe=False)


class Fell(generics.ListCreateAPIView):
    """
        get:
            Return a list of every fall records
        post:
            Create a new fall record
    """
    permission_classes = [IsAuthenticated]

    queryset = Fall.objects.all()
    serializer_class = FallSerializer


class FallDetails(generics.RetrieveUpdateDestroyAPIView):
    """
        get:
            Return an existing fall record
        put:
            Update an existing fall record
        patch:
            Partial Update an existing fall record
        delete:
            Delete an existing fall record
    """
    permission_classes = [IsAuthenticated]

    queryset = Fall.objects.all()
    serializer_class = FallSerializer


class ChildFell(generics.ListAPIView):
    """
        get:
            Return a list of all fall records for a certain child
    """
    serializer_class = FallSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        child = self.kwargs['child_id']
        return Fall.objects.filter(child_id=child)


class ChildLastFall(APIView):
    """Return last recorded Fall for a child"""
    permission_classes = [IsAuthenticated]

    def get(self, request, **kwargs):
        child = kwargs['child_id']
        query = Fall.objects.filter(child_id=child).latest('created_at')
        serializer = FallSerializer(query, many=False)
        return JsonResponse(serializer.data, safe=False)


class Signup(APIView):
    """
        SignUp endpoint.
    """
    def post(self, request, **kwargs):

        mac = request.data["mac_address"]

        try:
            child_name = request.data["child_name"]
            child_birth = request.data["child_birth_date"]
        except KeyError:
            raspberry = RaspberryPi.objects.filter(mac_address=mac)

            if not raspberry:
                return JsonResponse({"response": "Mac Address not found"}, status=status.HTTP_404_NOT_FOUND)

            child = raspberry[0].child

            username = request.data['username']
            password = request.data['password']

            user = User.objects.create_user(username=username, password=password)
            user.save()

            parent = Parent(user=user, child=child)
            parent.save()

            return JsonResponse({"response": "Singed up successfully"}, status=status.HTTP_201_CREATED)


        child = Child(name=child_name, birth_date=child_birth)
        child.save()

        raspberry = RaspberryPi.objects.filter(mac_address=mac)

        if not raspberry:
            return JsonResponse({"response": "Mac Address not found"}, status=status.HTTP_404_NOT_FOUND)

        raspberry[0].child = child
        raspberry[0].save()

        username = request.data['username']
        password = request.data['password']

        user = User.objects.create_user(username=username, password=password)
        user.save()

        parent = Parent(user=user, child=child)
        parent.save()

        return JsonResponse({"response": "Singed up successfully"}, status=status.HTTP_201_CREATED)


class SignIn(APIView):
    """
        SignIn Endpoint
    """
    def post(self, request, **kwargs):
        username = request.data['username']
        password = request.data['password']

        user = authenticate(request, username=username, password=password)

        if user is None:
            return Response("Invalid data", status=status.HTTP_401_UNAUTHORIZED)

        tokens = jwt_views.TokenObtainPairView.as_view()(request=request._request).data

        p = Parent.objects.filter(user=user)
        raspberry = RaspberryPi.objects.filter(child_id=p[0].child.id)
        res = {"child-id": p[0].child.id, "mac_address": raspberry[0].mac_address, "refresh": tokens['refresh'], "access": tokens["access"]}

        return JsonResponse(res)
