from django.contrib import admin

from api.models import Temperature, PulseRate, Child, Fall, RaspberryPi, Parent

admin.site.register(RaspberryPi)
admin.site.register(Temperature)
admin.site.register(PulseRate)
admin.site.register(Parent)
admin.site.register(Child)
admin.site.register(Fall)
