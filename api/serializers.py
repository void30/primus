from rest_framework import serializers
from api.models import Temperature, PulseRate, Child, Fall, RaspberryPi


class TemperatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Temperature
        fields = '__all__'


class PulseRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PulseRate
        fields = '__all__'


class ChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Child
        fields = '__all__'


class FallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fall
        fields = '__all__'


class RaspberryPiSerializer(serializers.ModelSerializer):
    class Meta:
        model = RaspberryPi
        fields = '__all__'
