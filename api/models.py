from django.db import models
from django.contrib.auth.models import User


class Child(models.Model):
    name = models.CharField(max_length=50, null=False)
    birth_date = models.DateField(auto_now_add=False, auto_now=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Child"
        verbose_name_plural = "Children"


class RaspberryPi(models.Model):
    mac_address = models.CharField(max_length=50, null=False, unique=True)
    VPN_ip = models.CharField(max_length=12, null=False)

    child = models.ForeignKey(Child, related_name='child_pi', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.mac_address

    class Meta:
        verbose_name = "Raspberry Pi"
        verbose_name_plural = "Raspberry Pis"


class Temperature(models.Model):
    temp = models.DecimalField(max_digits=4, decimal_places=2, null=False)
    created_at = models.DateTimeField(auto_now=True)

    child = models.ForeignKey(Child, related_name='child_temperature', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.temp

    class Meta:
        ordering = ['-created_at']

        verbose_name = "Temperature"
        verbose_name_plural = "Temperature"


class PulseRate(models.Model):
    pulse_rate = models.IntegerField(null=False)
    created_at = models.DateTimeField(auto_now=True)

    child = models.ForeignKey(Child, related_name='child_pulse_rates', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.pulse_rate

    class Meta:
        ordering = ['-created_at']

        verbose_name = "Pulse Rate"
        verbose_name_plural = "Pulse Rates"


class Fall(models.Model):
    child_fell = models.BooleanField(null=False)
    created_at = models.DateTimeField(auto_now=True)

    child = models.ForeignKey(Child, related_name='child_fall', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.child_fell

    class Meta:
        ordering = ['-created_at']

        verbose_name = 'Child fall'
        verbose_name_plural = 'Child fall'


class Parent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="parent")
    child = models.ForeignKey(Child, related_name='child_parent', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
